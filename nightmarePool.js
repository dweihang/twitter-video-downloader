const NightmareIntance = require('./nightmareInstance');

var instancePool = {}

const getIdleInstance = (device) => {
  var deviceFarm = instancePool[device.id];
  if (deviceFarm) {
    const len = deviceFarm.length;
    var count = 0;
    while (count < len) {
      if (!deviceFarm[count].busy) {
        deviceFarm[count].busy = true;
        return deviceFarm[count].instance;
      }
      count = count + 1;
    }
  }
  return null;
}

module.exports = {
  get: (device) => {
    var instance = getIdleInstance(device);
    if (instance) {
      return instance;
    }
    instance = new NightmareIntance(device);
    var deviceId = device.id;
    if (!instancePool[deviceId]) {
      instancePool[deviceId] = [];
    }
    var instancePoolForDevice = instancePool[deviceId];
    instancePoolForDevice.push({
      instance: instance,
      busy: true,
      created: new Date().getTime()
    });
    instance.once('did-start-loading', () => {
      instance.proc.once('close', ()=> {
        var newInstancePoolForDevice = [];
        for (var i = 0, len = instancePoolForDevice.length; i < len; i++) {
          if (instancePoolForDevice[i].instance === instance) {
            continue;
          }
          newInstancePoolForDevice.push(instancePoolForDevice[i]);
        }

        instancePool[deviceId] = newInstancePoolForDevice;
      });
    });
    return instance;
  },

  recycle: (instance) => {
    var deviceFarm = instancePool[instance.device.id];
    if (deviceFarm) {
      var len = deviceFarm.length;
      while (len--) {
        if (deviceFarm[len].instance === instance) {
          deviceFarm[len].busy = false;
          return true;
        }
      }
    }
    return false;
  }
}
