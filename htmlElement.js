/**
 * HTMLElement - Description
 *
 * @param {String} tag      Description
 * @param {Object} attrs    Description
 * @param {String} text     Description
 * @param {Object} parent   Description
 * @param {Array_Object} children Description
 *
 * @return {type} Description
 */
function HTMLElement(tag, attrs, text, parent, children) {
  this.tag = tag;
  this.attrs = attrs;
  this.text = text;
  this.parent = parent;
  this.children = [] || children;
}

/**
 * setParent - Description
 *
 * @param {Object} parent Description
 *
 * @return {This} Description
 */
HTMLElement.prototype.setParent = function(parent) {
  this.parent = parent;
  return this;
}

/**
 * getParent - Description
 *
 * @return {Object} Description
 */
HTMLElement.prototype.getParent = function() {
  return this.parent;
}

/**
 * setTag - Description
 *
 * @param {String} tag Description
 *
 * @return {This} Description
 */
HTMLElement.prototype.setTag = function(tag) {
  this.tag = tag;
  return this;
}

/**
 * getTag - Description
 *
 * @return {String} Description
 */
HTMLElement.prototype.getTag = function() {
  return this.tag;
}

/**
 * setAttributes - Description
 *
 * @param {Object} attrs Description
 *
 * @return {This} Description
 */
HTMLElement.prototype.setAttributes = function(attrs) {
  this.attrs = attrs;
  return this;
}

/**
 * getAttributes - Description
 *
 * @return {Object} Description
 */
HTMLElement.prototype.getAttributes = function() {
  return this.attrs;
}

/**
 * setText - Description
 *
 * @param {String} text Description
 *
 * @return {This} Description
 */
HTMLElement.prototype.setText = function(text) {
  this.text = text;
  return this;
}

/**
 * getText - Description
 *
 * @return {String} Description
 */
HTMLElement.prototype.getText = function() {
  return this.text;
}

/**
 * appendChild - Description
 *
 * @param {Object} childElement Description
 *
 * @return {This} Description
 */
HTMLElement.prototype.appendChild = function(childElement) {
  if (childElement instanceof HTMLElement) {
    this.children.push(childElement);
  }
  return this;
}

module.exports = HTMLElement;
