/**
 * isValidUrl - Description
 *
 * @param {String} str Description
 *
 * @return {Boolean} Description
 */
function isValidUrl(str) {
	var pattern = new RegExp('^(https?:\\/\\/)'+ // protocol
	  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
	  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
	  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
	  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
	  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  if (!pattern.test(str)) {
    return false;
  } else {
    return true;
  }
}



/**
 * assembleFullUrl - Description
 *
 * @param {String} baseUrl Description
 * @param {String} urlPath Description
 *
 * @return {String} Description
 */
function assembleFullUrl(baseUrl, urlPath) {
	if (urlPath.startsWith('http')) {
		return urlPath;
	} else {
		var _baseUrl = (baseUrl[baseUrl.length - 1] == '/' ?  baseUrl.slice(0, -1) : baseUrl);
		if (urlPath.length == 0) {
			return _baseUrl;
		}
		var _urlPath = (urlPath[0] == '/' ?  urlPath.substring(1) : urlPath);
		return _baseUrl + '/' + _urlPath;
	}
}

module.exports = {
  isValidUrl : isValidUrl,
  assembleFullUrl : assembleFullUrl
}
