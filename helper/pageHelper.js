const nightmarePool = require('../nightmarePool');

module.exports = {
  dumpPage: (pageUrl, device) => {
    const browser = nightmarePool.get(device);
    return browser.goto(pageUrl).evaluate(()=> {
      return document.documentElement.innerHTML;
    });
  }
}
