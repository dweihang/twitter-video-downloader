var mysql = require('mysql');
var util = require('util');
var Promise = require('bluebird');

Promise.promisifyAll(mysql);
Promise.promisifyAll(require("mysql/lib/Connection").prototype);
Promise.promisifyAll(require("mysql/lib/Pool").prototype);

var pool = mysql.createPool({
    connectionLimit: 5,
    host: 'localhost',
    user: 'root',
    password: process.env.MSYQL_PWD,
    database: 'twittervdo',
    supportBigNumbers: true,
    bigNumberStrings: true,
    charset : 'utf8mb4'
});

/**
 * escape - escape content to avoid sql injection
 *
 * @param {String} val original content
 *
 * @return {String} escaped content
 */
function escape(val) {
  return pool.escape.call(pool, val);
}

/**
 * getSqlConnection - get sql connection
 *
 * @return {Object} Description
 */
function getSqlConnection() {
  return pool.getConnectionAsync().disposer(function(connection) {
        connection.release();
  });
}

/**
 * query - perform sql query
 *
 * @param {String} queryString query string
 *
 * @return {Promise} Description
 */
function query(queryString) {
  return Promise.using(getSqlConnection(), function(connection) {
    return connection.queryAsync(queryString);
  }).catch(function(e) {
    console.log(e);
    throw e;
  });
}

/**
 * transaction - Description
 *
 * @param {type} fn Description
 *
 * @return {type} Description
 */
function transaction(fn) {
  return Promise.using(getSqlConnection(), function(connection) {
    var tx = connection.beginTransaction();
    return Promise
      .try(fn, tx)
      .then(
        function(res) {
          return connection.commitAsync().then(function(res) {
            return res;
          })
        },
      function(err) {
        console.log(err);
        return connection.rollbackAsync().catch(function(e) {
          throw err;
        });
      });
  });
}

module.exports = {
  "query" : query,
  "transaction" : transaction,
  "escape" : escape
}
