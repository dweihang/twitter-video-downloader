const nightmarePool = require('./nightmarePool');
const devices = require('./devices');
const urlHelper = require('./helper/urlHelper');

const playBtnSelector = 'div[aria-label="Play this video"] svg circle';
const videoSelector = 'video';

module.exports = {
  findVideoUrl: (pageUrl) => {
    const browser = nightmarePool.get(devices.getDeviceById("iphone_5"));
    var videoResolveTask = new Promise((resolve, reject) => {
      var timeoutMilli = 5000;
      var timeout;
      const _browser_event_callback = (e, s, url, o, rc, rm, r, h, type) => {
        if (!timeout) {
          timeout = setTimeout(() => {
            browser.removeListener('did-get-response-details', _browser_event_callback);
            var recycled = nightmarePool.recycle(browser);
            if (!recycled) {
              browser.halt('nightmare not being recycled, killing the process');
            }
            console.log(`timeout after:${timeoutMilli}, recycled nightmare successfully`);
            reject(new Error(`timeout:${timeoutMilli} for retriving video url for page:${pageUrl}`));
          }, timeoutMilli);
        }
        if (type != 'image' && type != 'xhr' && url && url.indexOf('.mp4') >= 0) {
          clearTimeout(timeout);
          browser.removeListener('did-get-response-details', _browser_event_callback);
          var recycled = nightmarePool.recycle(browser);
          if (!recycled) {
            browser.halt('nightmare not being recycled, killing the process');
          }
          console.log(`get videoUrl:${url} for pageUrl:${pageUrl}, recycled nightmare successfully`);
          resolve(url);
        }
      };
      browser.on('did-get-response-details', _browser_event_callback);
    });

    if (pageUrl.indexOf('/video/') <= 0) {
      return browser
        .goto(pageUrl)
        .wait(playBtnSelector)
        .evaluate(() => {
          return document
              .querySelector('title')
              .textContent;
        })
        .then((video_desc) => {
          return browser
            .click(playBtnSelector)
            .wait(videoSelector)
            .wait((videoSelector) => {
              var video = document.querySelector(videoSelector);
              return video.readyState > 0
            }, videoSelector)
            .evaluate((videoSelector) => {
              var videoElement = document.querySelector(videoSelector);
              return {
                cover_image_url: videoElement.getAttribute('poster'),
                video_duration: parseInt(videoElement.duration)
              }
            }, videoSelector)
            .then((ret) => {
              return videoResolveTask.then((result) => {
                ret.video_url = result;
                ret.video_desc = video_desc;
                console.log(ret);
                return ret;
              });
            });
        });
    }
  },

  isValidTwitterUrl: (url) => {
    if (!urlHelper.isValidUrl(url)) {
      return false;
    }
    if (url.indexOf('twitter.') < 0 || url.indexOf('/status/') < 0) {
      return false;
    }
    return true;
  }
}
