const Nightmare = require('nightmare');

const resetUrl = "http://www.google.com";

function NightmareInstance(device) {
  Nightmare.call(this, {
    'show': true,
    'switches': {
      'ignore-certificate-errors': true,
      'disable-http2': true
    },
    'waitTimeout': 5000,
    'executionTimeout': 5000
  });

  this.device = device;
  this
    .useragent(device.UA)
    .viewport(device.viewport.width, device.viewport.height);
  this.lastUrlVisited = null;
}

NightmareInstance.prototype = Object.create(Nightmare.prototype);
NightmareInstance.prototype.constructor = NightmareInstance;
NightmareInstance.prototype.goto = function(url){
  if (this.lastUrlVisited == url) {
    Nightmare
    .prototype
    .goto
    .call(this, resetUrl);
  }
  this.lastUrlVisited = url;
  return Nightmare
  .prototype
  .goto
  .call(this, url);
}

module.exports = NightmareInstance;
