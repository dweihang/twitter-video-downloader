var util = require('util');
var mysqlClient = require('../helper/mysqlHelper');
var Promise = require('bluebird');
var squel = require('squel');
var querystring = require('querystring');
var DbSavable = require('./DbSavable');


function PageActivityLog() {
  DbSavable.call(this);
  this.setTableName()

  this.id;
  this.initial_url;
  this.traverse_url;
  this.html_content;
  this.status_code;
  this.created_at;
}

util.inherits(PageActivityLog, DbSavable);
