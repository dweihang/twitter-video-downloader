var mysqlClient = require('../helper/mysqlHelper');
var squel = require('squel');

var DbSavable = (function() {
  var _tableName = null;

  function DbSavable() {}

  DbSavable.prototype.getTableName = function () {
      return _tableName;
  }

  DbSavable.prototype.setTableName = function (tableName) {
      _tableName = tableName;
  }

  DbSavable.prototype.toJsonEntity = function() {
    var ret = {};
    var _this = this;
    Object.getOwnPropertyNames(this).forEach(function(property) {
      var val = _this[property]
      if (val != undefined && val != null) {
        ret[property] = mysqlClient.escape(val);
      }
    });
    return ret;
  }

  DbSavable.prototype.save = function() {
    var insertBody = this.toJsonEntity();

    var sqlQueryBuilder = squel
    .insert()
    .into(_tableName)
    .setFieldsRows([
      insertBody
    ]);

    return mysqlClient.query(sqlQueryBuilder.toString());
  }

  return DbSavable;
})();

module.exports = DbSavable;
