const devices = [
  {
    id: "mac",
    UA: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like " +
        "Gecko) Chrome/60.0.3112.72 Safari/537.36",
    viewport: {
      width: 1280,
      height: 800
    }
  }, {
    id: "nexus",
    UA: "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML" +
        ", like Gecko) Chrome/60.0.3112.72 Mobile Safari/537.36",
    viewport: {
      width: 1080,
      height: 1920
    }
  }, {
    id: "iphone_5",
    UA: "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTM" +
        "L, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1",
    viewport: {
      width: 320,
      height: 568
    }
  }, {
    id: "ipad_pro",
    UA: "Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like G" +
        "ecko) Version/9.0 Mobile/13B143 Safari/601.1",
    viewport: {
      width: 1024,
      height: 1366
    }
  }
];

var id2deviceMap = {};
for (var i = 0, len = devices.length; i < len; i++) {
  id2deviceMap[devices[i].id] = devices[i];
}

module.exports = {
  getDeviceById: (id) => {
    return id2deviceMap[id];
  }
}
