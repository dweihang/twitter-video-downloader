var express = require('express');
var app = express();
var Promise = require('bluebird');
var path = require('path');
var twitterVideoScrapper = require('./twitter');
var facebookVideoScrapper = require('./facebook');
var youtubeVideoScrapper = require('./youtube');
var vineVideoScrapper = require('./vineVideoScrapper');
var instagramVideoScrapper = require('./instagram');

var bodyParser = require('body-parser');

// parse various different custom JSON types as JSON
app.use(bodyParser.json());

app.get('/index', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.post('/video', function(req, res) {
  var targetUrl = req.body.url;
  if (targetUrl) {
    if (twitterVideoScrapper.isValidTwitterUrl(targetUrl)) {
      twitterVideoScrapper
      .findVideoUrl(targetUrl)
      .then(function(ret) {
        res.json({
          "code" : 200,
          "message" : 'fetch url ok',
          "data" : ret
        });
      })
      .catch(function(e) {
        console.log(e);
        res.json({
          "code" : 400,
          "message" : 'fail to fetch url'
        })
      });
    }
    else if (facebookVideoScrapper.isValidFacebookUrl(targetUrl)) {
      facebookVideoScrapper
      .findVideoUrl(targetUrl)
      .then(function(ret) {
        res.json({
          "code" : 200,
          "message" : 'fetch url ok',
          "data" : ret
        });
      })
      .catch(function(e) {
        console.log(e);
        res.json({
          "code" : 400,
          "message" : 'fail to fetch url'
        })
      });
    }
    else if (youtubeVideoScrapper.isValidYoutubeUrl(targetUrl)) {
      youtubeVideoScrapper
      .findVideoUrl(targetUrl)
      .then(function(ret) {
        res.json({
          "code" : 200,
          "message" : 'fetch url ok',
          "data" : ret
        });
      })
      .catch(function(e) {
        console.log(e);
        res.json({
          "code" : 400,
          "message" : 'fail to fetch url'
        })
      });
    }
    else if (vineVideoScrapper.isValidVineUrl(targetUrl)) {
      vineVideoScrapper
      .findVideoUrl(targetUrl)
      .then(function(ret) {
        res.json({
          "code" : 200,
          "message" : 'fetch url ok',
          "data" : ret
        });
      })
      .catch(function(e) {
        console.log(e);
        res.json({
          "code" : 400,
          "message" : 'fail to fetch url'
        })
      });
    }
    else if (instagramVideoScrapper.isValidInstagramUrl(targetUrl)) {
      instagramVideoScrapper
      .findVideoUrl(targetUrl)
      .then(function(ret) {
        res.json({
          "code" : 200,
          "message" : 'fetch url ok',
          "data" : ret
        });
      })
      .catch(function(e) {
        console.log(e);
        res.json({
          "code" : 400,
          "message" : 'fail to fetch url'
        })
      });
    }
    else {
      res.json({
        "code" : 400,
        "message" : 'fail to resole url'
      })
    }
  }
});

app.listen(3456);
