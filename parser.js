var htmlparser = require("htmlparser2");
var HTMLElement = require('./htmlElement');

/**
 * Parser - Description
 *
 * @return {Class} Description
 */
function Parser() {
	if (!(this instanceof Parser)) return new Parser();
	this._domObject = {};
	this._rawHtml = "";
}

/**
 * parse - Description
 *
 * @param {type} htmlContent Description
 *
 * @return {type} Description
 */
Parser.prototype.parse = function(htmlContent) {
	this._rawHtml = htmlContent;
	var _this = this;
	return new Promise(function(resolve, reject) {
		var parser = new htmlparser.Parser(new htmlparser.DomHandler(function(err, dom) {
			if (err) {
				reject(err);
			}
			else {
				_this._domObject = dom;
				resolve(_this);
			}
		}), {decodeEntities: true});
			parser.write(htmlContent);
			parser.end();
		})
		.catch(function(e) {
			console.log(e);
			throw e;
		});
}

/**
 * getRawHTMLContent - Description
 *
 * @return {String} Description
 */
Parser.prototype.getRawHTMLContent = function() {
	return this._rawHtml;
}

/**
 * getDomObject - Description
 *
 * @return {Object} Description
 */
Parser.prototype.getDomObject = function() {
	return this._domObject;
}

/**
 * findAllTagsByTagNames - Description
 *
 * @param {String} tagname Description
 *
 * @return {Array_Object} Description
 */
Parser.prototype.findAllTagsByTagNames = function(tagname, domObj) {
	var _domObject = this._domObject;
	if (domObj) {
		_domObject = domObj;
	}
	return htmlparser.DomUtils.findAll(function(item) {
		if (item.name = tagname) {
			return item;
		}
	}, _domObject)
}


/**
 * findElementsByCondition - Description
 *
 * @param {type} fn Description
 *
 * @return {type} Description
 */
Parser.prototype.findElementsByCondition = function(fn, domObj) {
	var _domObject = this._domObject;
	if (domObj) {
		_domObject = domObj;
	}
	return htmlparser.DomUtils.findAll(function(item) {
		return fn(item);
	}, _domObject)
}

module.exports = Parser;