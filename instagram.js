const nightmarePool = require('./nightmarePool');
const devices = require('./devices');
const urlHelper = require('./helper/urlHelper');

const playBtnSelector = 'div[role="button"] a[role="button"]';
const videoSelector = 'video';
const coverImgSelector = 'meta[property="og:image"]';

module.exports = {
  findVideoUrl: (pageUrl) => {
    const browser = nightmarePool.get(devices.getDeviceById("iphone_5"));
    var videoResolveTask = new Promise((resolve, reject) => {
      var timeoutMilli = 5000;
      var timeout;
      const _browser_event_callback = (e, s, url, o, rc, rm, r, h, type) => {
        if (!timeout) {
          timeout = setTimeout(() => {
            browser.removeListener('did-get-response-details', _browser_event_callback);
            var recycled = nightmarePool.recycle(browser);
            reject(new Error(`timeout:${timeoutMilli} for retriving video url for page:${pageUrl}`));
          }, timeoutMilli);
        }
        if (type != 'image' && type != 'xhr' && url && url.indexOf('.mp4') >= 0) {
          clearTimeout(timeout);
          browser.removeListener('did-get-response-details', _browser_event_callback);
          var recycled = nightmarePool.recycle(browser);
          console.log(`recycled nightmare success:${recycled}`);
          resolve(url);
        }
      };
      browser.on('did-get-response-details', _browser_event_callback);
    });

    return browser
      .goto(pageUrl)
      .wait(playBtnSelector)
      .click(playBtnSelector)
      .wait((videoSelector) => {
        var video = document.querySelector(videoSelector);
        return video.readyState > 0
      }, videoSelector)
      .evaluate((videoSelector, coverImgSelector) => {
        var videoElement = document.querySelector(videoSelector);
        return {
          video_desc: document
            .querySelector('title')
            .textContent,
          cover_image_url: document
            .querySelector(coverImgSelector)
            .getAttribute('content'),
          video_duration: parseInt(videoElement.duration)
        }
      }, videoSelector, coverImgSelector)
      .then((ret) => {
        return videoResolveTask.then((result) => {
          ret.video_url = result;
          return ret;
        });
      });
  },

  isValidInstagramUrl: (url) => {
    if (!urlHelper.isValidUrl(url.trim())) {
      return false;
    }
    if (url.indexOf('instagram.') < 0) {
      return false;
    }
    return true;
  }
}
