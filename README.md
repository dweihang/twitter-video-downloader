## Scrapping the video url of a tweet

### Installation on Ubuntu

```
sudo apt-get update && sudo apt-get install -y xvfb x11-xkb-utils xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic x11-apps clang libdbus-1-dev libgtk2.0-dev libnotify-dev libgnome-keyring-dev libgconf2-dev libasound2-dev libcap-dev libcups2-dev libxtst-dev libxss1 libnss3-dev gcc-multilib g++-multilib
```

### Run on Ubuntu

```
DEBUG=nightmare xvfb-run --server-args="-screen 0 375x667x24" node index.js
```

### Run on Ubuntu with PM2

```
pm2 start process.json
```
