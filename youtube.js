const nightmarePool = require('./nightmarePool');
const devices = require('./devices');
const urlHelper = require('./helper/urlHelper');

const videoCoverRegex = 'url\(.*\);'

module.exports = {
  findVideoUrl: (pageUrl) => {
    const browser = nightmarePool.get(devices.getDeviceById("iphone_5"));
    var videoResolveTask = new Promise((resolve, reject) => {
      var timeoutMilli = 5000;
      var timeout;
      const _browser_event_callback = (e, s, url, o, rc, rm, r, h, type) => {
        if (!timeout) {
          timeout = setTimeout(() => {
            browser.removeListener('did-get-response-details', _browser_event_callback);
            var recycled = nightmarePool.recycle(browser);
            console.log(`recycled nightmare success:${recycled}`);
            reject(new Error(`timeout:${timeoutMilli} for retriving video url for page:${pageUrl}`));
          }, timeoutMilli);
        }
        if (type != 'image' && type != 'xhr' && url && url.indexOf('googlevideo.') >= 0) {
          clearTimeout(timeout);
          browser.removeListener('did-get-response-details', _browser_event_callback);
          var recycled = nightmarePool.recycle(browser);
          console.log(`recycled nightmare success:${recycled}`);
          resolve(url);
        }
      };
      browser.on('did-get-response-details', _browser_event_callback);
    });

    return browser
      .goto(pageUrl)
      .wait('video.video-stream.html5-main-video')
      .wait('div.ytp-cued-thumbnail-overlay-image')
      .wait('div.ytp-cued-thumbnail-overlay-duration')
      .wait('button.ytp-large-play-button')
      .click('button.ytp-large-play-button')
      .evaluate((videoCoverRegex) => {
        var durationText = document
          .querySelector('div.ytp-cued-thumbnail-overlay-duration')
          .textContent;
        var duration = 0;
        var hourMiniuteSeconds = durationText.split(':');
        for (var i = 0, len = hourMiniuteSeconds.length; i < len; i++) {
          duration += hourMiniuteSeconds[len - i - 1] * Math.pow(60, i);
        }
        var ret = {
          video_desc: document
            .querySelector('video.video-stream.html5-main-video')
            .getAttribute("title"),
          video_duration: parseInt(duration)
        }
        var regex = new RegExp(videoCoverRegex);
        var coverImageMatches = regex.exec(document.querySelector('div.ytp-cued-thumbnail-overlay-image').getAttribute('style'))
        ret.cover_image_url = document
          .querySelector('div.ytp-cued-thumbnail-overlay-image')
          .getAttribute('style');
        if (coverImageMatches && coverImageMatches.length > 1) {
          ret.cover_image_url = coverImageMatches[1]
            .slice(2)
            .slice(0, -2);
        }
        return ret;
      }, videoCoverRegex)
      .then((ret) => {
        return videoResolveTask.then((result) => {
          ret.video_url = result;
          return ret;
        });
      });
  },

  isValidYoutubeUrl: (url) => {
    if (!urlHelper.isValidUrl(url.trim())) {
      return false;
    }
    if (url.indexOf('youtube.') < 0 && url.indexOf('youtu') < 0) {
      return false;
    }
    return true;
  }
}
