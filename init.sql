CREATE DATABASE IF NOT EXISTS twittervdo DEFAULT CHARACTER SET utf8mb4;

USE twittervdo;

-- declare drop foreign key procedure
DROP PROCEDURE IF EXISTS drop_foreign_key;

DELIMITER $$

CREATE PROCEDURE drop_foreign_key(IN tableName VARCHAR(64), IN constraintName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.table_constraints
        WHERE
            table_schema    = DATABASE()     AND
            table_name      = tableName      AND
            constraint_name = constraintName AND
            constraint_type = 'FOREIGN KEY')
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP FOREIGN KEY ', constraintName, ';');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END$$

DELIMITER $$

DROP PROCEDURE IF EXISTS add_index $$
CREATE PROCEDURE add_index(in theTable varchar(128), in theIndexName varchar(128), in theIndexColumns varchar(128)  )
BEGIN
 IF((SELECT COUNT(*) AS index_exists FROM information_schema.statistics WHERE TABLE_SCHEMA = DATABASE() and table_name =
theTable AND index_name = theIndexName)  = 0) THEN
   SET @s = CONCAT('CREATE INDEX ' , theIndexName , ' ON ' , theTable, '(', theIndexColumns, ')');
   PREPARE stmt FROM @s;
   EXECUTE stmt;
 END IF;
END $$


CREATE TABLE IF NOT EXISTS `page_activity_log_tbl` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'fetch id',
  `initial_url` varchar(100) NOT NULL COMMENT 'initial url to fetch video',
  `traverse_url` varchar(100) DEFAULT NULL COMMENT 'url to traverse',
  `html_content` varchar(2000) DEFAULT NULL COMMENT 'html returned',
  `status_code` int NOT NULL COMMENT 'status code',
  `success` tinyint(1) NOT NULL COMMENT 'whether fetched success'
  `created_at` int NOT NULL COMMENT 'timestamp',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='html content';
