var Nightmare = require('nightmare');


var UA_MAC = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.72 Safari/537.36";
var VIEWPORT_MAC = "1280, 800";

var UA_NEXUS = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.72 Mobile Safari/537.36";
var VIEWPORT_NEXUS = "1080, 1920";

var UA_IPHONE_5 = "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1"
var VIEWPORT_IPHONE_5 = "320, 568";

var UA_IPAD_PRO = "Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1";
var VIEWPORT_IPAD_PRO = "1024, 1366"

var DEFAULT_UA = UA_MAC;
var DEFAULT_VIEWPORT = VIEWPORT_MAC;

var waitTime = 2500;

/**
 * Scrapper - Description
 *
 * @return {Class} Description
 */
function Scrapper() {
  var viewport = DEFAULT_VIEWPORT.split(',').map(function(item) {
    return parseInt(item.trim());
  });

  this._scrapperImpl = Nightmare({
    show: true,
    switches: {
       'ignore-certificate-errors': true,
       'disable-http2': true
    }
  });
}

/**
 * click - Description
 *
 * @return {This} Description
 */
Scrapper.prototype.click = function(selector) {
  this._scrapperImpl
  .click(selector)
  .then(function(){})

  return this;
}


/**
 * scrap - Scrapping the html content for the url
 *sp
 * @param {String} url Description
 *
 * @return {Promise_String} Description
 */
Scrapper.prototype.scrap = function(url, isHd) {
  var viewport;
  var ua;
  if (isHd) {
    viewport = VIEWPORT_IPAD_PRO.split(',').map(function(item) {
      return parseInt(item.trim());
    });
    ua = UA_IPAD_PRO;
  }
  else {
    viewport = VIEWPORT_IPHONE_5.split(',').map(function(item) {
      return parseInt(item.trim());
    });
    ua = UA_IPHONE_5;
  }

  return this._scrapperImpl
  .useragent(ua)
  .viewport(viewport[0], viewport[1])
  .cookies.clear()
  .goto(url)
  .wait(waitTime)
  .evaluate(function(){
	    return document.documentElement.innerHTML;
	})
}

Scrapper.prototype.listen = function(fn) {
  return this._scrapperImpl
  .on('did-get-response-details', function(e, s, n, o, rc, rm, r, h, type) {
    fn({url:n, type:type});
  });

  return this;
}

/**
 * free - release the electron process
 *
 * @return {Void} Description
 */
Scrapper.prototype.free = function() {
  if (this._scrapperImpl) {
    this._scrapperImpl.end().then(function(){});
    this._scrapperImpl = null;
  }
}

module.exports = Scrapper;
