var Scrapper = require('./scrapper');
var Parser = require('./parser');
const { URL } = require('url');
var UrlHelper = require('./helper/urlHelper');
var Promise = require('bluebird');
var HtmlEnDecodeHelper = require('./helper/htmlEnDecodeHelper');

/**
 * findPlayButton - Description
 *
 * @param {type} parser Description
 *
 * @return {type} Description
 */
function findPlayButton(parser) {
  var possibleElements = parser.findElementsByCondition(function(item) {
    var attribs = item.attribs;
    if (item.name == 'button' && attribs['class'] == 'play') {
      return true;
    }
    return false;
	});
  if (possibleElements.length > 0) {
    return possibleElements[0];
  }
  return null;
}

/**
 * isValidVineUrl - Description
 *
 * @param {String} url Description
 *
 * @return {Boolean} Description
 */
function isValidVineUrl(url) {
	if (!UrlHelper.isValidUrl(url.trim())) {
		return false;
	}
	if (url.indexOf('vine.') < 0) {
		return false;
	}
	return true;
}

function findCoverImageUrl(parser) {
	var possibleElements = parser.findElementsByCondition(function(item) {
			if (item.name == 'img' && item.attribs['class'] == 'thumbnail') {
				return true;
			}
			return false;
		});
	if (possibleElements.length == 1) {
		return HtmlEnDecodeHelper.decode(possibleElements[0].attribs['src']);
	}
	return null;
}

function findVideoDesc(parser) {
	var found = parser.getRawHTMLContent().match(/<title[^>]*>([^<]+)<\/title>/);
	if (found.length > 1) {
		return found[1];
	}
	return null;
}

/**
 * findVideoUrl - Description
 *
 * @param {String} url      Description
 * @param {Object} scrapper Description
 *
 * @return {String} Description
 */
function findVideoUrl(url, scrapper) {
	if (!isValidVineUrl(url)) {
		return Promise.reject('please enter a valid url');
	}

	var baseUrl =  new URL(url).origin;

	var _scrapper;

	if (scrapper) {
		_scrapper = scrapper;
	} else {
		_scrapper = new Scrapper();
	}

	return _scrapper
	.scrap(url)
	.then(function(htmlContent) {
		var parser = new Parser();
		return parser.parse(htmlContent);
	})
	.then(function(parser) {
		var playButton = findPlayButton(parser);
		if (playButton) {
			var coverImageUrl  = findCoverImageUrl(parser);
			var videoDesc = findVideoDesc(parser);
			return new Promise(function(resolve, reject) {
				_scrapper
				.listen(function(item){
					var timeout = setTimeout(function(){
						reject('click html element' + "." + playButton.attribs['class'] +  ' timeout');
					}, 5000);
					if (item.type != 'image' && item.type != 'xhr' && item.url.indexOf('.mp4')>=0) {
						clearTimeout(timeout);
						_scrapper.free();
						var ret = {};
						ret.video_url = item.url;
						if (coverImageUrl) {
							ret.cover_image_url = coverImageUrl;
						}
						if (videoDesc) {
							ret.video_desc = videoDesc;
						}
						resolve(ret);
					}
				});
        var possibleClazzes = playButton.attribs['class'].split(" ");
        for (var i=0; i < possibleClazzes.length; i++) {
          _scrapper.click("." + possibleClazzes[i]);
        }
			});
		}
		return Promise.reject('fail to find video');
	})
	.catch(function(e) {
		console.log(e);
		if (_scrapper) {
			_scrapper.free();
		}
		return Promise.reject(e);
	});
}

process.argv.forEach(function (val, index, array) {
  if (val == 'dumppage') {
    var sampleUrl = "https://vine.co/v/MqTwvtx6lAl";
    var targetHtmlFileName = "vinevdo.html"
    var scrapper = new Scrapper();
    scrapper
    .scrap(sampleUrl)
    .then(function(htmlContent) {
      return new Promise(function(resolve, reject) {
        var fs = require('fs');
        var path = require('path');
        fs.writeFile(path.join(__dirname, "/test", targetHtmlFileName), htmlContent, function(err) {
          if (err) {
            return reject(err);
          }
          resovle('done');
        })
      })
    })
    .then(function(){
      scrapper.free();
      console.log('done');
    })
    .catch(function(e){
      console.log(e);
    });
  }
});


module.exports = {
	'findVideoUrl' : findVideoUrl,
	'isValidVineUrl' : isValidVineUrl
}
