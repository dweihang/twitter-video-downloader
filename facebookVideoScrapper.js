var Scrapper = require('./scrapper');
var Parser = require('./parser');
const { URL } = require('url');
var UrlHelper = require('./helper/urlHelper');
var HtmlEnDecodeHelper = require('./helper/htmlEnDecodeHelper');
var Promise = require('bluebird');


/**
 * findPlayButton - Description
 *
 * @param {type} parser Description
 *
 * @return {type} Description
 */
function findPlayButton(parser, isHdVersion) {
  var possibleElements = parser.findElementsByCondition(function(item) {
		if (!isHdVersion) {
			var attribs = item.attribs;
			if (attribs['class']) {
					for (var prop in attribs) {
						if (attribs.hasOwnProperty(prop)) {
							if (attribs[prop].indexOf('play-button') >= 0) {
								return true;
							}
						}
					}
			}
		} else {
			var attribs = item.attribs;
			if (attribs['class']) {
					for (var prop in attribs) {
						if (attribs.hasOwnProperty(prop)) {
							if (attribs[prop].indexOf('Play Video') >= 0) {
								return true;
							}
						}
					}
			}
		}
    return false;
	});
  if (possibleElements.length == 1) {
    return possibleElements[0];
  }
  return null;
}

/**
 * 
 * @param {Object} parser 
 */
function findCoverImageUrl(parser) {
	var possibleElements = parser.findElementsByCondition(function(item) {
			if (item.name == 'meta' && item.attribs['property'] == 'og:image' && item.attribs['content']) {
				return true;
			}
			return false;
		});
	if (possibleElements.length == 1) {
		return HtmlEnDecodeHelper.decode(possibleElements[0].attribs['content']);
	}
	return null;
}


/**
 * 
 * @param {Object} parser 
 */
function findVideoDesc(parser) {
	var possibleElements = parser.findElementsByCondition(function(item) {
			if (item.name == 'meta' && item.attribs['property'] == 'og:description' && item.attribs['content']) {
				return true;
			}
			return false;
		});
	if (possibleElements.length == 1) {
		return HtmlEnDecodeHelper.decode(possibleElements[0].attribs['content']);
	}
	return null;
}

/**
 * isValidFacebookUrl - Description
 *
 * @param {String} url Description
 *
 * @return {Boolean} Description
 */
function isValidFacebookUrl(url) {
	if (!UrlHelper.isValidUrl(url.trim())) {
		return false;
	}
	if (url.indexOf('facebook.') < 0) {
		return false;
	}
	return true;
}

function findVideoUrl(url, scrapper) {
	if (!isValidFacebookUrl(url)) {
		return Promise.reject('please enter a valid url');
	}

	var baseUrl =  new URL(url).origin;

	var _scrapper;

	if (scrapper) {
		_scrapper = scrapper;
	} else {
		_scrapper = new Scrapper();
	}

	return _scrapper
	.scrap(url, true)
	.then(function(htmlContent) {
		var parser = new Parser();
		return parser.parse(htmlContent);
	})
	.then(function(parser) {
		var playButton = findPlayButton(parser, true);
		if (playButton) {
			var coverImageUrl  = findCoverImageUrl(parser);
			var videoDesc = findVideoDesc(parser);
			return new Promise(function(resolve, reject) {
				_scrapper
				.listen(function(item){
					console.log(item);
					var timeout = setTimeout(function(){
						reject('click html element' + "." + playButton.attribs['class'] +  ' timeout');
					}, 5000);
					if (item.type != 'image' && item.type != 'xhr' && item.url.indexOf('.mp4')) {
						clearTimeout(timeout);
						_scrapper.free();
						var ret = {};
						ret.video_url = item.url;
						if (coverImageUrl) {
							ret.cover_image_url = coverImageUrl;
						}
						if (videoDesc) {
							ret.video_desc = videoDesc;
						}
						resolve(ret);
					}
				});

				var possibleClazzes = playButton.attribs['class'].split(" ");
				for (var i=0; i < possibleClazzes.length; i++) {
					_scrapper.click("." + possibleClazzes[i]);
				}
			});
		}
		return Promise.reject('fail to find video');
	})
	.catch(function(e) {
		console.log(e);
		if (_scrapper) {
			_scrapper.free();
		}
		return Promise.reject(e);
	});
}

process.argv.forEach(function (val, index, array) {
  if (val == 'dumppage') {
    var sampleUrl = "https://www.facebook.com/100most/videos/1302210169906627/";
    var targetHtmlFileName = "fbvdo.html"
    var scrapper = new Scrapper();
    scrapper
    .scrap(sampleUrl)
    .then(function(htmlContent) {
      return new Promise(function(resolve, reject) {
        var fs = require('fs');
        var path = require('path');
        fs.writeFile(path.join(__dirname, "/test", targetHtmlFileName), htmlContent, function(err) {
          if (err) {
            return reject(err);
          }
          resolve('done');
        })
      })
    })
    .then(function(){
      scrapper.free();
      console.log('done');
    })
    .catch(function(e){
      console.log(e);
    });
  }
});

module.exports = {
	'findVideoUrl' : findVideoUrl,
	'isValidFacebookUrl' : isValidFacebookUrl
}
